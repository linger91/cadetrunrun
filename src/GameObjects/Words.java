package GameObjects;

import Application.GameMaster;
import org.academiadecodigo.simplegraphics.pictures.Picture;
import java.util.ConcurrentModificationException;
import java.util.Random;

public class Words implements Runnable{

    private volatile Picture wrdPicture;
    private final int gameSpeed = GameMaster.GAMESPEED;
    private volatile boolean shouldStartNewThread;

    public Words(String wordType, int backgroundWidth) {

        wrdPicture = new Picture(0, 0, wordType);
        wrdPicture.grow(-145, 0);
        wrdPicture.grow(0, -20);
        wrdPicture.translate(backgroundWidth- wrdPicture.getMaxX(),
                (int) (Math.ceil(new Random().nextInt(wrdPicture.getMaxY()+110, 300))));

        this.shouldStartNewThread = false;
    }

    public boolean getShouldStartNewThread(){

        return this.shouldStartNewThread;
    }

    @Override
    public void run() {

        wrdPicture.draw();
        while (wrdPicture.getX() > 10) {

            if (wrdPicture.getX() <= 50) {
                shouldStartNewThread = true;
            }

            try {
                try{
                    wrdPicture.translate(-10, 0);
                    Thread.sleep(gameSpeed);
                }catch (ConcurrentModificationException e) {
                    continue;
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt(); // restore interrupted status
                }
            } catch (ConcurrentModificationException e) {
                continue;
            }
        }

        if (wrdPicture.getX() <= 10) {

                wrdPicture.delete();
        }
    }
}