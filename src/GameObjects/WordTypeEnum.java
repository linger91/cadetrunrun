package GameObjects;

public enum WordTypeEnum {

    GUESSANUMBER("resources/backgrounds/word_guessNumber.png"),
    ARABIANNIGHTS("resources/backgrounds/word_arabianNights.png"),
    CARCRASH("resources/backgrounds/word_carCrash.png"),
    ELITESNIPER("resources/backgrounds/word_eliteSniper.png"),
    MAPEDITOR("resources/backgrounds/word_mapEditor.png"),
    ROCKPAPERSCISSORS("resources/backgrounds/word_rockPaperScissors.png");

    private String pictureAdress;

    WordTypeEnum(String pictureAdress) {
        this.pictureAdress = pictureAdress;
    }

    public String getPicture() {
        return pictureAdress;
    }
}

