package GameObjects;

import Application.GameMaster;
import Entities.Player;
import Misc.Messages;
import Stages.Stage;
import Stages.StageMaster;
import org.academiadecodigo.simplegraphics.pictures.Picture;
import java.util.ConcurrentModificationException;

public class Obstacle implements Runnable {

    private volatile boolean hasCrashed;
    private volatile Picture obsPicture;
    private int gameSpeed = GameMaster.GAMESPEED;
    private volatile boolean shouldStartNewThread;
    private Player player;
    private StageMaster sm;

    public Obstacle(String obsTye, boolean hasCrashed, int backgroundWidth, int gameFloor, Player player, StageMaster sm) {

        this.sm = sm;
        this.obsPicture = new Picture(0,0, obsTye);
        try {
            this.obsPicture.translate(backgroundWidth - this.obsPicture.getMaxX(), gameFloor - this.obsPicture.getMaxY()-25);
        } catch (ConcurrentModificationException e) {
            //continue
        }

        this.hasCrashed = hasCrashed;
        this.shouldStartNewThread = false;
        this.player = player;
    }

    public void setHasCrashed(boolean x){

        hasCrashed = x;
    }

    public boolean isHasCrashed() {

        return hasCrashed;
    }

    public Picture getObsPicture(){

        return obsPicture;
    }

    public boolean getShouldStartNewThread() {
        return shouldStartNewThread;
    }

    @Override
    public void run() {

        CollisionDetector cd = new CollisionDetector(player , this);

        obsPicture.draw();
        while (obsPicture.getX() > 10 && !player.isPlayerDead()) {

            if (cd.isCollided()) {

                setHasCrashed(true);
                this.obsPicture.delete();
                player.takeDamage();
            }

            if (obsPicture.getX() <= 500) {
                shouldStartNewThread = true;
            }

            try {
                obsPicture.translate(-10, 0);
                Thread.sleep(gameSpeed);
            } catch (ConcurrentModificationException e) {
                continue;
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }


        }

        //Deletes pic when reach to the end of canvas left
        if (obsPicture.getX() <= 10 && !isHasCrashed()) {
            for (int i = 0; i < 3; i++) {
                try {
                    Thread.sleep(50);
                    obsPicture.delete();
                    Thread.sleep(50);
                    obsPicture.draw();
                } catch (ConcurrentModificationException e) {
                    continue;
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
            obsPicture.delete();
        }
    }
}