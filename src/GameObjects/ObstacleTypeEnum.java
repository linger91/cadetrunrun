package GameObjects;

public enum ObstacleTypeEnum {

    //DESK("desk-obstacle.png"), //is too big
    CHAIR("resources/obstacles/obs_chair.png"),
    YOGABALL("resources/obstacles/obs_yogaball.png"),
    LOLLYPOP("resources/obstacles/obs_lolipop.png"),
    PIZZA("resources/obstacles/obs_pizza.png"),
    FRENCHFRIES("resources/obstacles/obs_frenchFries.png");

    private String picture;

    ObstacleTypeEnum(String picture) {

        this.picture = picture;
    }

    public String getPicture() {
        return picture;
    }

}
