package GameObjects.Factories;

import Application.GameMaster;
import Controllers.ThreadController;
import Entities.Player;
import GameObjects.Obstacle;
import GameObjects.ObstacleTypeEnum;
import Stages.GamePlay;
import Stages.StageMaster;
import Stages.StagesEnum;

public class ObstacleFactory implements Runnable{

    private int backgroundWidth;
    private int gameFloor = GameMaster.GAMEFLOOR;
    private Obstacle actualObstacle;
    private StageMaster sm;
    private Player player;

    public ObstacleFactory(int backgroundWidth, StageMaster sm, Player player){

        this.backgroundWidth = backgroundWidth;
        this.sm = sm;
        this.player = player;
    }

    public String randomObject() {

        int random = (int) (Math.ceil(Math.random() * ObstacleTypeEnum.values().length-1));
        ObstacleTypeEnum obstacleType = ObstacleTypeEnum.values()[random];

        return obstacleType.getPicture();
    }

    public void createNewObject(String obstacleType){

        Obstacle obs = new Obstacle(obstacleType, false, backgroundWidth, gameFloor, player, sm);

        Thread threadObs = new Thread(obs);
        ThreadController.addToContainer(threadObs);
        threadObs.start();

        actualObstacle = obs;
    }

    @Override
    public void run() {

        while(!actualObstacle.getShouldStartNewThread()){

            synchronized (this) {
                sm.changeStage();
            }
        }

        if (sm.canSendObstacles()) sm.obstacleDispenser();
    }
}