package GameObjects.Factories;

import GameObjects.WordTypeEnum;
import GameObjects.Words;
import Stages.StageMaster;

public class WordFactory implements Runnable{


    private int backgroundWidth;
    private Words actualWord;
    private StageMaster sm;

    public WordFactory(int backgroundWidth, StageMaster sm){

        this.backgroundWidth = backgroundWidth;
        this.sm = sm;
    }

    public String randomObject() {

        int random = (int) (Math.ceil(Math.random() * WordTypeEnum.values().length-1));
        WordTypeEnum wordType = WordTypeEnum.values()[random];

        return wordType.getPicture();
    }

    public void createNewWord(String wordType){

        Words wrd = new Words(wordType, backgroundWidth);

        Thread threadObs = new Thread(wrd);
        threadObs.start();

        actualWord = wrd;
    }

    @Override
    public void run() {

        while(!actualWord.getShouldStartNewThread()){}

        sm.wordsDispenser();

    }
}

