package GameObjects;

import Entities.Player;

public class CollisionDetector {

    private Player player;
    private Obstacle obstacle;

    public CollisionDetector(Player player, Obstacle obstacle){

        this.player = player;
        this.obstacle = obstacle;
    }

    public boolean isCollided() {

        if (!player.canJump()) return false;

        if(!obstacle.isHasCrashed()) {

            if ((player.getPlayerPicture().getMaxX() - 30) >= obstacle.getObsPicture().getX() &&
                    player.getPlayerPicture().getX() <= (obstacle.getObsPicture().getMaxX() - 30) &&
                    player.getPlayerPicture().getY() - player.getPlayerPicture().getHeight() <= obstacle.getObsPicture().getY()) {

                obstacle.setHasCrashed(true);
                return true;
            }
        }
        return false;
    }
}