package Misc;

import Stages.GamePlay;
import Stages.Stage;
import Stages.StageMaster;
import Stages.StartMenu;
import org.academiadecodigo.simplegraphics.graphics.Text;

import java.util.ConcurrentModificationException;

public class Score implements Runnable {

    private int currentScore;
    private Stage actualStage;

    public Score(){

        this.currentScore = 0;
    }

    public int getCurrentScore() {

        return currentScore;
    }

    public void scoreCounter(Stage actualStage) {

        this.actualStage = actualStage;

        Thread scoreCount = new Thread(this);
        scoreCount.start();
    }

    public void resetScore(){

        this.currentScore = 0;
    }

    @Override
    public void run() {

        Text currentScorePresenting = new Text(15, 15, "");
        Text highestScorePresenting = new Text(15, 25, "");

        while (actualStage instanceof GamePlay) {
            try {
                try{
                    currentScore++;
                    Thread.sleep(100);

                    //Draw and Update Scores on Screen while playing:
                    currentScorePresenting.setText(Messages.INFO_CURRENT_SCORE + currentScore);
                    currentScorePresenting.draw();
                    highestScorePresenting.setText(Messages.INFO_HIGHEST_SCORE + SaveGame.getInstance().loadScores());
                    highestScorePresenting.draw();
                } catch (ConcurrentModificationException e) {
                    continue;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
                System.out.println(Messages.FAILED_COUNTING_SCORES);
            }
        }

        if(actualStage instanceof StartMenu) {
            resetScore();
        }
    }
}