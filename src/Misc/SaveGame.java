package Misc;

import java.io.*;

public class SaveGame {

    private static final String PATH = "resources/saveData.txt";
    private int highScore;
    private static SaveGame saveGame;

    private SaveGame(){

        this.highScore = loadScores();
    }


    public static SaveGame getInstance(){
        if(saveGame == null){
            saveGame = new SaveGame();
        }

        return saveGame;
    }

    public void saveScore(int score) {

        try {
            BufferedWriter bWriter = new BufferedWriter(new FileWriter(PATH));

            if(score > highScore){

                bWriter.write(String.valueOf(score));
            } else {

                bWriter.write(String.valueOf(this.highScore));
            }

            bWriter.flush();
            bWriter.close();

        } catch (IOException e) {

            e.printStackTrace();
        }
    }

    public int loadScores(){

        String bufferReader = "";
        String m = "";

        try {
            BufferedReader reader = new BufferedReader(new FileReader(PATH));

            while((bufferReader = reader.readLine()) != null){

                m += bufferReader;
            }
            reader.close();

        } catch (FileNotFoundException e) {
            return 0;
        } catch (IOException e) {
            return 0;
        }

        return Integer.valueOf(m);
    }
}
