package Misc;

public class GameLevels {

    private int lastScore;
    private int presentScore;
    private int gameLevel;

    public GameLevels(int score) {

        this.presentScore = score;
        this.lastScore = presentScore;
        this.gameLevel = 1;
    }

    public void setLevel(){

        if (presentScore - lastScore >= 100) {
            gameLevel ++;
            lastScore = presentScore;
        }
    }

    public int getLevel(){

        return this.gameLevel;
    }

}
