package Misc;

public class Messages {

    public final static String PLAYER_IS_JUMPING = "Player is jumping!";
    public final static String PLAYER_IS_NOT_JUMPING = "Player is not jumping!";
    public final static String FAILED_TO_JUMP = "Failed to Jump!";
    public final static String FAILED_COUNTING_SCORES = "Score counting error.";
    public final static String INFO_HIGHEST_SCORE = "Highest Score: ";
    public final static String INFO_CURRENT_SCORE = "Current Score: ";

    public final static String INTERRUPTED_OBSTACLE_ANNIMATION = "[Thread Stopped]: Obstacle Animation";
    public final static String INTERRUPTED_RUNNING_ANNIMATION = "[Thread Stopped]: Player Animation";
}
