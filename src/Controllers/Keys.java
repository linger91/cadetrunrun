package Controllers;

import Application.GameMaster;
import Entities.PlayerMovements;
import Multimedia.JumpSound;
import Multimedia.StageSounds;
import Stages.*;
import org.academiadecodigo.simplegraphics.keyboard.*;

public class Keys implements KeyboardHandler {

    private Keyboard keyboard;
    private GameMaster game;
    PlayerMovements playerMovements;

    public Keys(GameMaster game){

        this.keyboard = new Keyboard(this);
        this.game = game;
        this.playerMovements = new PlayerMovements(game.getPlayer());

        createKeyboardEvents();
    }

    private void createKeyboardEvents() {

        int[] keys = {
                KeyboardEvent.KEY_RIGHT,
                KeyboardEvent.KEY_LEFT,
                KeyboardEvent.KEY_SPACE,
                KeyboardEvent.KEY_Q,
                KeyboardEvent.KEY_R,
                KeyboardEvent.KEY_M
        };

        for (int key : keys) {
            KeyboardEvent event = new KeyboardEvent();
            event.setKey(key);
            event.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
            keyboard.addEventListener(event);
        }

        KeyboardEvent event = new KeyboardEvent();
        event.setKey(KeyboardEvent.KEY_SPACE);
        event.setKeyboardEventType(KeyboardEventType.KEY_RELEASED);
        keyboard.addEventListener(event);
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {

        switch (keyboardEvent.getKey()) {

            case KeyboardEvent.KEY_SPACE:
                if (game.getActualStage() instanceof StartMenu) {
                    game.setNextStage(StagesEnum.GAME_PLAY);
                    game.startStage();
                } else if(game.getActualStage() instanceof GamePlay){
                    playerMovements.jump();
                }
                break;

            case KeyboardEvent.KEY_RIGHT:
                playerMovements.moveRight();
                break;

            case KeyboardEvent.KEY_LEFT:
                playerMovements.moveLeft();
                break;

            case KeyboardEvent.KEY_Q: //

                if (game.getActualStage() instanceof StartMenu || game.getActualStage() instanceof EndGame) {
                    System.exit(0);
                }

                ThreadController.stopThreads();
                game.setNextStage(StagesEnum.START_MENU);
                game.startStage();
                break;

            case KeyboardEvent.KEY_R: //TODO: RESTART is OK?!?!

                if(game.getActualStage() instanceof EndGame) {

                    game.getActualStage().resetStage();
                    game.getActualStage().startStage();
                }
                break;

            case KeyboardEvent.KEY_M:

                StageSounds.getInstance().startStopMusic();
                JumpSound.getInstance().startStopMusic();
                break;
        }
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {

    }
}