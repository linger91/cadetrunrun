package Controllers;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author tiago.esteves
 */
public class ThreadController {

    static Set<Thread> threadContainer= new LinkedHashSet<>();

    /**
     * @method to store the Threads created during the game
     * @param thread
     */
    public static synchronized void addToContainer(Thread thread) {

        threadContainer.add(thread);
    }

    /**
     * @method to stop & remove the threads currently inside Set
     */
    public static synchronized void stopThreads(){

        for(Thread thread : threadContainer){
            thread.interrupt();
        }

        threadContainer.removeAll(threadContainer);
    }
}
