package Multimedia;

public enum SoundsEnums {

    START_MENU("resources/music/snd_startMenu.wav"),
    GAME_PLAY("resources/music/snd_gamePlay.wav"),
    JUMP_PLAY("resources/music/snd_jump.wav"),
    END_GAME("resources/music/snd_restart.wav"),
    RESTART_GAME("resources/music/snd_restart.wav");

    private String sound;

    SoundsEnums(String sound) {
        this.sound = sound;
    }

    public String getSound(){
        return sound;
    }

}