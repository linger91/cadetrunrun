package Multimedia;

import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Background {

    private Picture picture;

    public Background(String imgString) {

        this.picture = new Picture(10, 10, imgString);
    }

    public Picture getPicture(){

        return this.picture;
    }

    public void load(String picture){

        this.picture.load(picture);
    }

    public void draw() {

        picture.draw();
    }

    public void delete(){

        picture.delete();
    }


}
