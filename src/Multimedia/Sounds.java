package Multimedia;

public interface Sounds {

    void setMusic(String sound);

    void startMusic();

    void stopMusic();

    void startStopMusic();
}
