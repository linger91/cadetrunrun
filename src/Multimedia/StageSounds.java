package Multimedia;

import javax.sound.sampled.*;
import java.io.File;
import java.io.IOException;

public class StageSounds implements Sounds {

    private static StageSounds stageSounds;
    private Clip music;
    private AudioInputStream audioInputStream;

    private StageSounds(){

        try {
            audioInputStream = AudioSystem.getAudioInputStream(new File(SoundsEnums.START_MENU.getSound()));
        } catch (UnsupportedAudioFileException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static StageSounds getInstance(){
        if(stageSounds == null) {
            stageSounds = new StageSounds();
        }

        return stageSounds;
    }

    @Override
    public void setMusic(String sound) {
        //URL wavFile = getClass().getClassLoader().getResources(sound);

        try {
            audioInputStream = AudioSystem.getAudioInputStream(new File(sound));
        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            music = AudioSystem.getClip();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void startMusic() {

        if(music.isRunning()) stopMusic();

        try {
            music.open(audioInputStream);
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        music.start();
    }

    @Override
    public void stopMusic() {

        if(music == null) {
            setMusic(SoundsEnums.START_MENU.getSound());
        }

        music.stop();
    }

    @Override
    public void startStopMusic() {

        if(music.isRunning()) {
            music.stop();
        } else {
            music.start();
        }
    }
}