package Multimedia;

import javax.sound.sampled.*;
import java.io.File;
import java.io.IOException;

public class JumpSound implements Sounds {

    private static JumpSound sounds;
    private Clip music;
    private AudioInputStream audioInputStream;
    private boolean playerSetMusicOFF;

    private JumpSound(){

        playerSetMusicOFF = false;

        try {
            audioInputStream = AudioSystem.getAudioInputStream(new File(SoundsEnums.JUMP_PLAY.getSound()));
        } catch (UnsupportedAudioFileException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    public static JumpSound getInstance(){
        if(sounds == null) {
            sounds = new JumpSound();
        }

        return sounds;
    }

    @Override
    public void setMusic(String sound) {
        //URL wavFile = getClass().getClassLoader().getResources(sound);

        try {
            audioInputStream = AudioSystem.getAudioInputStream(new File(sound));
        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            music = AudioSystem.getClip();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void startMusic() {

        if(!playerSetMusicOFF) {

            if (music.isRunning()) stopMusic();

            try {
                music.open(audioInputStream);
            } catch (LineUnavailableException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            music.start();
        }
    }

    @Override
    public void stopMusic() {

        if(music == null) {
            setMusic(SoundsEnums.START_MENU.getSound());
        }

        music.stop();
    }

    @Override
    public void startStopMusic() {

        if(!playerSetMusicOFF) {
            music.stop();
            playerSetMusicOFF = true;
        } else {
            music.start();
            playerSetMusicOFF = false;
        }
    }
}