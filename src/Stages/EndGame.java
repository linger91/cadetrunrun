package Stages;

import Misc.*;
import org.academiadecodigo.simplegraphics.graphics.Text;

public class EndGame extends StageController {

    private StageMaster sm;

    public EndGame(StageMaster sm) {

        this.sm = sm;
        actualStage = this;
    }


    @Override
    public void startStage() {

        actualStage = this;

        setMusic();
        setBackground();
        showFinalScore();
        saveScores();

        sm.getPlayer().resetPlayer();
        sm.getScores().resetScore();
    }

    @Override
    public void resetStage() {

        actualStage = sm.getInstanceOfGamePlay();

        System.out.println(actualStage);
        getActualStage().resetStage();
    }

    private void showFinalScore() {

        Text currentScorePresenting = new Text(100, 100, "");
        Text highestScorePresenting = new Text(100, 175, "");

        currentScorePresenting.setText(Messages.INFO_CURRENT_SCORE + sm.getPlayer().getPlayerScore().getCurrentScore());
        currentScorePresenting.draw();
        highestScorePresenting.setText(Messages.INFO_HIGHEST_SCORE + SaveGame.getInstance().loadScores());
        highestScorePresenting.draw();
    }

    @Override
    public void setBackground() {

        sm.getBackground().delete();
        sm.getBackground().load(StagesEnum.END_GAME.getPicture());
        sm.getBackground().draw();
    }

    @Override
    public void setMusic() {

        sm.getStageSounds().stopMusic();
    }

    public void saveScores() {

        SaveGame.getInstance().saveScore(sm.getPlayer().getPlayerScore().getCurrentScore());
    }
}