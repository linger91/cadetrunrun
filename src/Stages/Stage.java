package Stages;

public interface Stage {

    void startStage();

    void resetStage();

    void setBackground();

    void setMusic();

    Stage getActualStage();


}
