package Stages;

import Controllers.ThreadController;
import Multimedia.SoundsEnums;

public class StartMenu extends StageController {

    private StageMaster sm;

    public StartMenu(StageMaster sm) {

        this.sm = sm;
        actualStage = this;
    }

    @Override
    public void setBackground() {

        sm.getBackground().getPicture().delete();
        sm.getBackground().load(StagesEnum.START_MENU.getPicture());
        sm.getBackground().draw();
    }

    @Override
    public void setMusic() {

        sm.getStageSounds().stopMusic();
        sm.getStageSounds().setMusic(SoundsEnums.START_MENU.getSound());
        sm.getStageSounds().startMusic();
    }

    @Override
    public void startStage() {

        actualStage = this;

        ThreadController.stopThreads();
        setBackground();
        setMusic();
        sm.getPlayer().resetPlayer();
    }

    @Override
    public void resetStage() {

        sm.getStageSounds().stopMusic();
        sm.getStageSounds().setMusic(SoundsEnums.RESTART_GAME.getSound());
        sm.getStageSounds().startMusic();
    }
}