package Stages;

public enum StagesEnum {

    START_MENU("resources/backgrounds/bck_startMenu.png"),
    GAME_PLAY("resources/backgrounds/bck_gamePlay.png"),
    END_GAME("resources/backgrounds/bck_endGame.png");

    private String picture;


    StagesEnum(String picture) {

        this.picture = picture;
    }

    public String getPicture() {
        return picture;
    }

}
