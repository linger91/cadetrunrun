package Stages;

import lombok.Getter;
import lombok.Setter;

public abstract class StageController implements Stage{

    /**
     * This Abstract class is created only
     * to have the variable controller actualStage
     * because if defined inside the interface it would be final and immutable
     */

    @Getter
    @Setter
    protected Stage actualStage;

}