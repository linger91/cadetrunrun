package Stages;

import Controllers.ThreadController;
import Entities.PlayerAnimation;
import Multimedia.SoundsEnums;
import lombok.Getter;
import lombok.Setter;

public class GamePlay extends StageController {

    private StageMaster sm;

    public GamePlay(StageMaster sm) {

        this.sm = sm;
        actualStage = this;
    }

    @Override
    public void startStage() {

        actualStage = this;

        setBackground();
        setMusic();
        showPlayer();
        startPlayerAnimation();
        showScores();

        //Create Thread for Object:
        sm.obstacleDispenser();

        //Create Words Thread Dispenser:
        sm.wordsDispenser();
    }

    @Override
    public void resetStage() {

        sm.setActualStage(sm.getInstanceOfEndGame());

        sm.resetWorldThreads();
        sm.getPlayer().resetPlayer();
        sm.getScores().resetScore();
    }

    public void startPlayerAnimation(){

        //Create Thread for Player:
        PlayerAnimation playerAnimation = new PlayerAnimation(sm.getPlayer(), sm);

        Thread playerRunning = new Thread(playerAnimation);
        ThreadController.addToContainer(playerRunning);
        playerRunning.start();
    }

    public void showPlayer(){

        sm.getPlayer().resetPlayer();
        sm.getPlayer().drawPlayer();
        sm.getPlayer().drawPlayerLives();
    }

    public void showScores(){

        sm.getScores().scoreCounter(sm.getActualStage());
    }

    @Override
    public void setBackground() {

        sm.getBackground().delete();
        sm.getBackground().load(StagesEnum.GAME_PLAY.getPicture());
        sm.getBackground().draw();
    }

    @Override
    public void setMusic() {

        sm.getStageSounds().stopMusic();
        sm.getStageSounds().setMusic(SoundsEnums.GAME_PLAY.getSound());
        sm.getStageSounds().startMusic();
    }

}
