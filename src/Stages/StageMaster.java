package Stages;

import Application.GameMaster;
import Controllers.ThreadController;
import Entities.*;
import GameObjects.Factories.ObstacleFactory;
import GameObjects.Factories.WordFactory;
import Misc.Score;
import Multimedia.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StageMaster{

    private Player player;
    private Background background;
    private StageSounds stageSounds;
    private Score scores;
    private GameMaster gm;
    private Stage actualStage;

    private StartMenu startMenu;
    private GamePlay gamePlay;
    private EndGame endGame;


    public StageMaster(Player player, Background background, StageSounds stageSounds, Score scores, GameMaster gm) {

        this.player = player;
        this.background = background;
        this.stageSounds = stageSounds;
        this.scores = scores;
        this.gm = gm;
    }

    public StartMenu getInstanceOfStartMenu() {

        if(startMenu == null) {
            startMenu = new StartMenu(this);
        }

        return startMenu;
    }

    public GamePlay getInstanceOfGamePlay() {

        if(gamePlay == null) {
            gamePlay = new GamePlay(this);
        }

        return gamePlay;
    }

    public EndGame getInstanceOfEndGame() {

        if(endGame == null) {
            endGame = new EndGame(this);
        }

        return endGame;
    }

    public void setNextStage(StagesEnum nextStage) {

        if(nextStage == StagesEnum.START_MENU) actualStage = getInstanceOfStartMenu();
        if(nextStage == StagesEnum.GAME_PLAY) actualStage = getInstanceOfGamePlay();
        if(nextStage == StagesEnum.END_GAME) actualStage = getInstanceOfEndGame();
    }

    public void obstacleDispenser(){

        ObstacleFactory obsFactory = new ObstacleFactory(background.getPicture().getWidth(), this, player);

        //TODO: delete comment
        System.out.println(obsFactory);

        obsFactory.createNewObject(obsFactory.randomObject());

        Thread observeObjectRunning = new Thread(obsFactory);
        ThreadController.addToContainer(observeObjectRunning);
        observeObjectRunning.start();

        System.out.println("New Obs Factory Thread Created: " + observeObjectRunning.getName());
    }

    public void wordsDispenser(){

        WordFactory wf = new WordFactory(background.getPicture().getWidth(), this);

        wf.createNewWord(wf.randomObject());

        Thread observeWordRunning = new Thread(wf);
        observeWordRunning.start();
    }


    public void resetWorldThreads() {

       ThreadController.stopThreads();
    }

    public void updateStage() {

        actualStage = actualStage.getActualStage();
    }

    public Stage getActualStage() {
        updateStage();

        return actualStage;
    }

    public void changeStage() {

        if(player.isPlayerDead() && actualStage == getInstanceOfGamePlay()) {

            actualStage = getInstanceOfEndGame();
            actualStage.startStage();
        }
    }

    public boolean canSendObstacles() {

        return !player.isPlayerDead() && actualStage == getInstanceOfGamePlay();
    }
}