package Entities;

import Application.GameMaster;
import Misc.Score;
import lombok.Getter;
import lombok.Setter;
import org.academiadecodigo.simplegraphics.pictures.Picture;
import java.util.ConcurrentModificationException;
import java.util.concurrent.CopyOnWriteArrayList;

public class Player {

    @Getter
    private Picture playerPicture;
    @Getter
    private Score playerScore;
    private Integer lives;
    private CopyOnWriteArrayList<Picture> playerLives;
    private static final Integer gameFloor = GameMaster.GAMEFLOOR;

    @Getter
    @Setter
    private boolean playerDead;


    public Player(Score score){

        this.playerScore = score;
        this.playerPicture = new Picture(50, 400, PlayerImgEnum.PLAYER_RIGHT.getPlayerImg());
        this.playerDead = false;
        this.lives = 3;

        setPlayerHeartsLivesImage(this.lives);
    }

    private void setPlayerHeartsLivesImage(Integer livesAmount) {

        playerLives = new CopyOnWriteArrayList();
        Integer setPosX = 10;

        for(Integer live = 0; live < livesAmount; live++) {

            Picture picture = new Picture(setPosX, 0, PlayerImgEnum.PLAYER_LIVES.getPlayerImg());
            picture.translate(0, picture.getMaxY());
            playerLives.add(picture);

            setPosX+=25;
        }
    }

    public void playerHide() {

        this.playerPicture.delete();
    }

    public void drawPlayer() {

        playerPicture.draw();
    }

    public boolean canJump(){

        return (playerPicture.getY() == 391);
    }

    public void drawPlayerLives(){

        playerLives.stream().forEach(picture -> picture.draw());
    }

    private void deletePlayerLives() {

        playerLives.stream().forEach(picture -> picture.delete());
    }

    private void resetLives(int livesAmount){

        this.lives = livesAmount;
        playerLives = null;
        setPlayerHeartsLivesImage(this.lives);
    }

    public void resetPlayer() {

        //Reset lives:
        deletePlayerLives();
        resetLives(3);

        this.playerPicture.translate(-playerPicture.getX(), -playerPicture.getY());
        this.playerPicture.translate(50, gameFloor - playerPicture.getHeight());

        //Reset Player Scores
        playerScore.resetScore();
        playerDead = false;

        playerHide();
    }

    public void takeDamage() {

        lives -= 1;

        if(playerLives.size() == 1) {

            playerDead = true;
            return;
        }

        deletePlayerLives();
        resetLives(lives);
        drawPlayerLives();

        try{
            try {
                for (int i = 0; i < 3; i++) {
                    Thread.sleep(50);
                    playerPicture.delete();
                    Thread.sleep(50);
                    playerPicture.draw();
                }

            } catch (InterruptedException e) {
                Thread.currentThread().interrupt(); // restore interrupted status
            }
        } catch (ConcurrentModificationException e) {
            playerPicture.draw();
        }
    }
}