package Entities;

import Controllers.ThreadController;
import Multimedia.JumpSound;
import Multimedia.StageSounds;
import Multimedia.SoundsEnums;
import org.academiadecodigo.simplegraphics.pictures.Picture;

import java.util.ConcurrentModificationException;

public class PlayerMovements implements Runnable {

    private Player player;
    private volatile Picture playerPicture;

    public PlayerMovements(Player player){

        this.player = player;
        this.playerPicture = player.getPlayerPicture();
    }

    public void moveLeft() {

        if (playerPicture.getX() > 10) playerPicture.translate(-20, 0);
    }

    public void moveRight() {

        if (playerPicture.getX() < 500) playerPicture.translate(10, 0);
    }

    public void jump() {

        if(player.canJump()) {

            JumpSound sound = JumpSound.getInstance();
            sound.setMusic(SoundsEnums.JUMP_PLAY.getSound());
            sound.startMusic();

            Thread jumping = new Thread(this);
            ThreadController.addToContainer(jumping);
            jumping.start();
        }
    }

    @Override
    public void run() {

        int jumpHeight = 80;
        int jumpSpeed = 35;

        //Going Up && Forward
        for(int i = 1 ; i < 11 ; i ++) {

            try {
                playerPicture.translate(8, -jumpHeight/i);
                Thread.sleep(jumpSpeed);
            }catch (ConcurrentModificationException e) {
                continue;
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt(); // restore interrupted status
            }
        }

        //Going Down
        for(int i = 10; i > 0 ; i --){
            try {
                playerPicture.translate(0, jumpHeight/i);
                Thread.sleep(jumpSpeed);
            } catch (ConcurrentModificationException e) {
                continue;
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt(); // restore interrupted status
            }
        }
    }
}