package Entities;

import Misc.Messages;
import Stages.GamePlay;
import Stages.StageMaster;

public class PlayerAnimation implements Runnable {

    private Player player;
    private StageMaster sm;

    public PlayerAnimation(Player player, StageMaster sm) {
        this.player = player;
        this.sm = sm;
    }

    @Override
    public void run() {

        while(sm.getActualStage() instanceof GamePlay && !Thread.currentThread().isInterrupted()) {

            try {
                Thread.sleep(150);
                player.getPlayerPicture().load(PlayerImgEnum.PLAYER_LEFT.getPlayerImg());
                Thread.sleep(150);
                player.getPlayerPicture().load(PlayerImgEnum.PLAYER_RIGHT.getPlayerImg());
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }
}