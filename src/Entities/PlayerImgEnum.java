package Entities;

public enum PlayerImgEnum {

    PLAYER_LEFT("resources/player/player_left.png"),
    PLAYER_RIGHT("resources/player/player_right.png"),
    PLAYER_CENTER("resources/player/player_center.png"),
    PLAYER_LIVES("resources/others/oth_lives.png");

    private String playerPos;

    PlayerImgEnum(String playerPos){

        this.playerPos = playerPos;
    }

    public String getPlayerImg() {

        return playerPos;
    }

}