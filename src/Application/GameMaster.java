package Application;


import Controllers.Keys;
import Entities.Player;
import Misc.Score;
import Multimedia.Background;
import Multimedia.StageSounds;
import Stages.Stage;
import Stages.StageMaster;
import Stages.StagesEnum;

public class GameMaster {

    public static final int GAMEFLOOR = 520;
    public static final int GAMESPEED = 50;
    private Keys keys;
    private Player player;
    private Background background;
    private StageSounds stageSounds;
    private Score scores;
    private StageMaster sm;

    GameMaster() {

        this.background = new Background("resources/backgrounds/bck_startMenu.png");
        this.stageSounds = StageSounds.getInstance();
        this.scores = new Score();
        this.player = new Player(this.scores);
        this.keys = new Keys(this);
        this.sm = new StageMaster(this.player, this.background, this.stageSounds, this.scores, this);

        sm.setActualStage(sm.getInstanceOfStartMenu());
    }

    public void startStage() {

        sm.getActualStage().startStage();
    }

    public Player getPlayer() {

        return player;
    }

    public Stage getActualStage(){

        return sm.getActualStage();
    }

    public void setNextStage(StagesEnum stageName){

        sm.setNextStage(stageName);
    }
}